import crypto from 'crypto';
import fs from 'fs';

const keyPair = crypto.generateKeyPairSync('rsa', {
    modulusLength: 4096, // bits - standard for RSA keys
    publicKeyEncoding: {
        type: 'pkcs1', // "Public Key Cryptography Standards 1" 
        format: 'pem' // Most common formatting choice
    },
    privateKeyEncoding: {
        type: 'pkcs1',
        format: 'pem'
    }
});
fs.writeFileSync('./id_rsa_pub.pem', keyPair.publicKey); 
fs.writeFileSync('./id_rsa.pem', keyPair.privateKey);
