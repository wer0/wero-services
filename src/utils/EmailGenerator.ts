import mjml2html from 'mjml';

type EmailType = 'VerifyEmail' | 'ResetPassword';
type Props = { [key: string]: any };

export default class EmailGenerator {
    
    private VerifyEmail (props: Props) {
        return `
            <mjml>
                <mj-body>
                    <mj-section>
                    <mj-column>
                        <mj-text font-size="20px" color="#ffc800"font-weight="bold">Wero</mj-text>
                        <mj-divider border-color="#ffc800"></mj-divider>
                        <mj-text font-size="20px">Email verification</mj-text>
                        <mj-text>Hello ${props.username}!</mj-text>
                        <mj-text>Please click the following link to activate your account on Wero.</mj-text>
                        <mj-button background-color="#ffc800" href="${props.verifyLink}">Activate</mj-button>
                    </mj-column>
                    </mj-section>
                </mj-body>
            </mjml>
        `
    }

    private ResetPassword (props: Props) {
        return `
            <mjml>
                <mj-body>
                    <mj-section>
                    <mj-column>
                        <mj-text font-size="20px" color="#ffc800"font-weight="bold">Wero</mj-text>
                        <mj-divider border-color="#ffc800"></mj-divider>
                        <mj-text font-size="20px">Password reset</mj-text>
                        <mj-text>Hello ${props.username}!</mj-text>
                        <mj-text>Please click the following link to set your new password.</mj-text>
                        <mj-button background-color="#ffc800" href="${props.resetLink}">Reset</mj-button>
                    </mj-column>
                    </mj-section>
                </mj-body>
            </mjml>
        `
    }

    public getHTML (type: EmailType, props: Props): string {
        const template = this[type](props);
        return mjml2html(template).html;
    }

}