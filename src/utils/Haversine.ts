// Stolen and simplified from https://github.com/dcousens/haversine-distance/blob/master/index.js

// Equatorial mean radius of Earth (in meters)
const R = 6378137

function squared (x: number) { return x * x }
function toRad (x: number) { return x * Math.PI / 180.0 }
function hav (x: number) {
  return squared(Math.sin(x / 2))
}

type Coordinates = [a: number, b: number]

// hav(theta) = hav(bLat - aLat) + cos(aLat) * cos(bLat) * hav(bLon - aLon)
function haversineDistance (a: Coordinates, b: Coordinates) {
  const aLat = toRad(a[1])
  const bLat = toRad(b[1])
  const aLng = toRad(a[0])
  const bLng = toRad(b[0])

  const ht = hav(bLat - aLat) + Math.cos(aLat) * Math.cos(bLat) * hav(bLng - aLng)
  return 2 * R * Math.asin(Math.sqrt(ht))
}

export default haversineDistance;