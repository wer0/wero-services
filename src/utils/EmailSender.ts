import mailjet from 'node-mailjet';
let mailjetClient: mailjet.Email.Client | undefined;

if(process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'development') {
  mailjetClient = mailjet.connect(process.env.MJ_API_KEY!, process.env.MJ_API_SECRET!)
}

export default class EmailSender {

    private recipientEmail: string;
    private subject: string;
    private html: string;
    
    constructor(recipientEmail: string, subject: string, html: string) {
        this.recipientEmail = recipientEmail;
        this.subject = subject;
        this.html = html;
    }

    public async send () {
        await mailjetClient!.post('send', {'version': 'v3.1'}).request({
            Messages: [
                {
                  "From": {
                    "Email": "wero@navelencia.com",
                    "Name": "Wero"
                  },
                  "To": [
                    {
                      "Email": this.recipientEmail,
                      "Name": this.recipientEmail
                    }
                  ],
                  "Subject": this.subject,
                  "TextPart": this.subject,
                  "HTMLPart": this.html,
                  "CustomID": "WeroServices"
                }
              ]
        });

    }

}