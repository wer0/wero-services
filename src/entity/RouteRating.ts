import { JoinColumn, ManyToOne } from "typeorm";
import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";
import { Route } from "./Route";
import { User } from "./User";

@Entity()
export class RouteRating {

    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    rating!: number;
    
    @ManyToOne(() => User)
    @JoinColumn()
    user!: User;

    @ManyToOne(() => Route)
    @JoinColumn()
    route!: Route;

    

}
