import { Entity, PrimaryGeneratedColumn, Column, OneToMany, OneToOne, JoinColumn } from "typeorm";
import { Roles } from "../server/Roles";
import { Route } from "./Route";
import { UserCredential } from "./UserCredential";

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id!: number;

    @Column({ unique: true })
    username!: string;

    @Column({ unique: true })
    email!: string;

    @Column()
    emailVerified!: boolean;

    @Column({ nullable: true })
    lastLoginDate!: Date;

    @Column()
    signUpDate!: Date;

    @OneToOne(() => UserCredential, { cascade: true })
    @JoinColumn()
    credential!: UserCredential;

    @Column()
    role!: Roles;
    
    @OneToMany(() => Route, Route => Route.user, { cascade: true })
    routes!: Route[];

}
