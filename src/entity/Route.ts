import { JoinColumn, OneToMany } from "typeorm";
import {Entity, PrimaryGeneratedColumn, Column, ManyToOne} from "typeorm";
import { User } from "./User";
import { RouteRating } from "./RouteRating";

@Entity()
export class Route {

    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    name!: string;

    @Column()
    description!: string;
    
    @Column({ nullable: true })
    gpx!: string;

    @Column({ nullable: true })
    geoJSON!: string;

    @Column({ nullable: true })
    distance!: number;

    @Column({ nullable: true })
    elevationGain!: number;

    @Column({ nullable: true })
    rating!: number;

    @ManyToOne(() => User, user => user.routes, { onDelete: 'SET NULL' })
    @JoinColumn()
    user!: User;

    @OneToMany(() => RouteRating, routeRating => routeRating.route, { cascade: true })
    ratings!: RouteRating[];
    
}
