import { Entity, PrimaryGeneratedColumn, Column, OneToOne } from "typeorm";
import { User } from "./User";

@Entity()
export class UserCredential {

    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    hash!: string;

    @Column()
    salt!: string;

    @OneToOne(() => User, { cascade: true, onDelete: 'CASCADE' })
    user!: User;

}
