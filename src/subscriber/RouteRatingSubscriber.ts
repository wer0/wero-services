import { EntitySubscriberInterface, EventSubscriber, InsertEvent, UpdateEvent } from "typeorm";
import { Route } from "../entity/Route";
import { RouteRating } from "../entity/RouteRating";

@EventSubscriber()
export class RouteRatingSubscriber implements EntitySubscriberInterface<RouteRating> {

    listenTo() {
        return RouteRating;
    }

    async afterInsert(event: InsertEvent<RouteRating>) {
        await this.computeRouteRateAverage(event);
    }

    async afterUpdate(event: UpdateEvent<RouteRating>) {
        await this.computeRouteRateAverage(event);
    }

    async computeRouteRateAverage (event: InsertEvent<RouteRating> | UpdateEvent<RouteRating>) {
        const routeRating = await event.manager.getRepository(RouteRating).findOne(event.entity.id, { relations: ['route'] });
        const route = await event.manager.getRepository(Route).findOne(routeRating?.route.id);
        if (route) {
            const res =  await event.manager.getRepository(RouteRating)
                .createQueryBuilder("rating")  
                .select("AVG(rating.rating)", "rating")
                .where("rating.routeId = :id", { id: route.id })
                .getRawOne();
            route.rating = Math.round(Number(res.rating));
            await event.manager.getRepository(Route).save(route);
        }
    }

}