import { Request } from "express";
import { readFileSync } from "fs";
import { PassportStatic } from "passport";
import { ExtractJwt, Strategy as JwtStrategy, StrategyOptions, VerifiedCallback } from "passport-jwt";
import { getRepository } from "typeorm";
import { User } from "../entity/User";

const publicKey = readFileSync('./id_rsa_pub.pem').toString();

function headerOrCookieExtractor (req: Request): string | null {
    const bearerTokenFromHeaders = ExtractJwt.fromAuthHeaderAsBearerToken()(req);
    if (bearerTokenFromHeaders) return bearerTokenFromHeaders;

    let jwt = null 
    if (req && req.cookies) {
        jwt = req.cookies.jwt;
    }

    return jwt;
}

export function configurePassport (passport: PassportStatic): void {

    const options: StrategyOptions = {
        jwtFromRequest: headerOrCookieExtractor,
        secretOrKey: publicKey,
        algorithms: ['RS256']
    }

    passport.use(new JwtStrategy(options, async (payload: { sub: string }, done: VerifiedCallback) => {
        try {
            const user = await getRepository(User).findOne(payload.sub);
            if (user && user.emailVerified === false) {
                return done(null, user, 'EMAIL_NOT_VERIFIED');
            } else if (user) {
                return done(null, user);
            } else {
                return done(null, false);
            }
        } catch (err) {
            done(err);
        }
    }));

}