import crypto from 'crypto';
import { readFileSync } from 'fs';
import { sign } from 'jsonwebtoken';
import { getRepository } from 'typeorm';
import { User } from '../entity/User';

const privateKey = readFileSync('./id_rsa.pem').toString();

export function validPassword(password: string, hash: string, salt: string): boolean {
    const hashVerify = crypto.pbkdf2Sync(password, salt, 10000, 64, 'sha512').toString('hex');
    return hash === hashVerify;
}

export function generateSaltAndHash(password: string): { salt: string, hash: string } {
    const salt = crypto.randomBytes(32).toString('hex');
    const hash = crypto.pbkdf2Sync(password, salt, 10000, 64, 'sha512').toString('hex');
    
    return {
      salt,
      hash
    };
}

function base64Encode (data: string): string {
  const buffer = Buffer.from(data);
  return buffer.toString('base64');
}

function base64Decode (data: string): string {
  const buffer = Buffer.from(data, 'base64');
  return buffer.toString('ascii');
}

export function generateUniqueAuthenticationToken (user: User): { identity: string, today: string, token: string } {
  const today = base64Encode(new Date().toISOString());
  const identity = base64Encode(String(user.id));

  const data = {
    today: today,
    userId: identity,
    lastLogin: user.lastLoginDate.toISOString(),
    hash: user.credential.hash,
    email: user.email
  };

  const token = crypto.pbkdf2Sync(JSON.stringify(data), String(process.env.SECRET), 10000, 64, 'sha512').toString('hex');
  return { identity, today, token };
}

export async function authenticateUniqueAuthenticationToken (identity: string, today: string, token: string): Promise<User | void> {
  const user = await getRepository(User).findOne(base64Decode(identity), { relations: ['credential'] });
  if (!user) return;

  const comparisonData = {
    today: today,
    userId: identity,
    lastLogin: user.lastLoginDate.toISOString(),
    hash: user.credential.hash,
    email: user.email
  }

  const comparisonToken = crypto.pbkdf2Sync(JSON.stringify(comparisonData), String(process.env.SECRET), 10000, 64, 'sha512').toString('hex');

  if (comparisonToken === token) return user;
  else return;
}

export function issueJWT(user: User): string {
  const _id = user.id;

  const expiresIn = '1d';

  const payload = {
    sub: _id,
    iat: Date.now()
  };

  const signedToken = sign(payload, privateKey, { expiresIn, algorithm: 'RS256' });

  return signedToken;
}