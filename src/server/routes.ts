enum ROUTES {
  HEALTH = "/health",
  USER = "/user",
  USER_BY_ID = "/user/:id",
  LOGIN = "/login",
  PASSWORD_FORGOT = "/password-forgot",
  PASSWORD_RESET = "/password-reset/:identity/:today/:token",
  VERIFY_EMAIL = "/verify-email/:identity/:today/:token",
  RESEND_VERIFICATION_EMAIL = "/resend-verification-email",

  ROUTE = "/route",
  ROUTE_BY_ID = "/route/:id",
  RATING = "/route/:id/rating",
  ROUTE_GPX_UPLOAD = "/route/:id/gpx-upload"
}

export default ROUTES;
