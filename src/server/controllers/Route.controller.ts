import * as toGeoJSON from '@mapbox/togeojson';
import { DOMParser } from 'xmldom';

import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { Route } from "../../entity/Route";
import { User } from "../../entity/User";
import haversineDistance from '../../utils/Haversine';
import { RouteRating } from '../../entity/RouteRating';
export class RouteController {
  public static async create(req: Request, res: Response): Promise<Response> {
    const user = await getRepository(User).findOne(req.user?.id);
    if (!user) return res.sendStatus(404);
    
    const route = new Route();
    route.name = req.body.name;
    route.description = req.body.description;
    route.user = user;
    const results = await getRepository(Route).save(route);

    return res.status(201).send(results);
  }

  public static async readOne(req: Request, res: Response): Promise<Response> {
    const route = await getRepository(Route).findOne(req.params.id, {
      relations: ['user'],
    });
    if (!route) return res.sendStatus(404);
    return res.send(route);
  }

  public static async read(req: Request, res: Response): Promise<Response> {
    const routes = await getRepository(Route).find({ select: ['id', 'name', 'description', 'user', 'distance', 'elevationGain', 'rating'] });
    return res.send(routes);
  }

  public static async gpxUpload(
    req: Request,
    res: Response
  ): Promise<Response> {
    if (req.file) {
      const route = await getRepository(Route).findOne(req.params.id);
      if (route) {
        const gpx = req.file.buffer.toString();
        const gpxAsDOM = new DOMParser().parseFromString(gpx);
        const geoJSON = toGeoJSON.gpx(gpxAsDOM);
        const { distance, elevationGain } = RouteController.computeDistanceAndElevationGain(geoJSON);

        getRepository(Route).merge(route, { gpx, geoJSON: JSON.stringify(geoJSON), distance, elevationGain });
        const results = await getRepository(Route).save(route);
        return res.send(results);
      } else {
        return res.sendStatus(404);
      }
    }
    return res.sendStatus(404);
  }

  public static async update(req: Request, res: Response): Promise<Response> {
    const route = await getRepository(Route).findOne(req.params.id);
    if (route) {
      getRepository(Route).merge(route, req.body);
      const results = await getRepository(Route).save(route);
      return res.send(results);
    } else {
      return res.sendStatus(404);
    }
  }

  public static async rate(req: Request, res: Response): Promise<Response> {
    const user = await getRepository(User).findOne(req.user?.id);
    const route = await getRepository(Route).findOne(req.params.id);
    if (!route || !user) return res.sendStatus(404);
    if (req.body.rating === undefined) return res.sendStatus(400);

    let routeRating;

    routeRating = await getRepository(RouteRating).findOne({ user, route });
    if (routeRating) {
      routeRating.rating = Number(req.body.rating);
    } else {
      routeRating = new RouteRating();
      routeRating.rating = Number(req.body.rating);
      routeRating.user = user;
      routeRating.route = route;
    }
    
    const routeRatingSaved = await getRepository(RouteRating).save(routeRating);
    return res.send(routeRatingSaved);
  }

  public static async delete(req: Request, res: Response): Promise<Response> {
    return res.send(await getRepository(Route).delete(req.params.id));
  }

  private static computeDistanceAndElevationGain (geoJSON: { [key: string]: any }): { distance: number, elevationGain: number } {
    let distance = 0;
    let elevationGain = 0;
    
    geoJSON.features[0].geometry.coordinates.reduce((a: any, b: any) => {
      distance = distance + haversineDistance(a, b);
      const elevationDifference = b[2] - a[2];
      if (elevationDifference > 0) elevationGain += b[2] - a[2];
      return b;
    }, geoJSON.features[0].geometry.coordinates[0]);
    return {
      distance: Math.round(distance),
      elevationGain: Math.round(elevationGain)
    };
  }

}
