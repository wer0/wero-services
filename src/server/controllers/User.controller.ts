import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { User } from "../../entity/User";
import { UserCredential } from "../../entity/UserCredential";
import EmailGenerator from "../../utils/EmailGenerator";
import EmailSender from "../../utils/EmailSender";
import {authenticateUniqueAuthenticationToken, generateSaltAndHash, generateUniqueAuthenticationToken, issueJWT, validPassword } from "../Password";
import { Roles } from "../Roles";

const SECURE_COOKIE = process.env.NODE_ENV === 'production' ? true : false;
const MAXAGE_COOKIE = 1000 * 60 * 60 * 24;
const DOMAIN_COOKIE = process.env.NODE_ENV === 'production' ? process.env.DOMAIN_COOKIE : undefined

function areEmailsEnabled (): boolean {
  return process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'development'
}

export class UserController {
  public static async create(req: Request, res: Response): Promise<Response> {
    const { salt, hash } = generateSaltAndHash(req.body.password);

    const username = req.body.username as string;
    const email = req.body.email as string;
    if (!username || !email) return res.sendStatus(400)

    let user;
    let savedUser;

    try {
      user = await getRepository(User).create({
        username,
        email,
        credential: { salt, hash },
        signUpDate: new Date(),
        lastLoginDate: new Date(),
        role: Roles.USER,
        emailVerified: !areEmailsEnabled()
      });
      savedUser = await getRepository(User).save(user);
    } catch (err) {
      return res.sendStatus(409);
    }
    
    const token = issueJWT(savedUser);

    if(areEmailsEnabled()) {
      const uniqueAuthenticationToken = generateUniqueAuthenticationToken(user);
      const template = (new EmailGenerator()).getHTML('VerifyEmail', {
        username: savedUser.username,
        verifyLink: `${process.env.WEBAPP_URL}/verify-email/${uniqueAuthenticationToken.identity}/${uniqueAuthenticationToken.today}/${uniqueAuthenticationToken.token}`
      });
      const emailSender = new EmailSender(savedUser.email, 'Verify', template);
      await emailSender.send();
    }

    return res
    .cookie('jwt', token, { httpOnly: true, secure: SECURE_COOKIE, maxAge: MAXAGE_COOKIE })
    .send({ username: user.username, id: user.id, token: `Bearer ${token}` });
  }

  public static async read(req: Request, res: Response): Promise<Response> {
    const users = await getRepository(User).find({ select: ['username', 'id', 'role'] });
    return res.send(users);
  }

  public static async readOne(req: Request, res: Response): Promise<Response> {
    const user = await getRepository(User).findOne(req.params.id, { select: ['username', 'id', 'role', 'emailVerified'] });
    if (!user) return res.sendStatus(404);
    return res.send(user)
  }

  public static async update(req: Request, res: Response): Promise<Response> {
    if (!req.user || Number(req.params.id) !== req.user.id) return res.sendStatus(401);

    const UserRepository = getRepository(User);

    const user = await UserRepository.findOne(req.params.id, { relations: ['credential'] });
    if (!user) return res.sendStatus(404);

    if (req.body.newPassword && req.body.oldPassword) {
      const oldPasswordIsValid = validPassword(req.body.oldPassword, user.credential.hash, user.credential.salt);
      if (!oldPasswordIsValid) return res.sendStatus(409);
      const saltAndHash = generateSaltAndHash(req.body.newPassword);
      await getRepository(UserCredential).update(user.credential.id, saltAndHash);
    }

    if (req.body.username || req.body.email) {
      const update: Partial<User> = { username: req.body.username, email: req.body.email };
      UserRepository.merge(user, update);
      await UserRepository.save(user);
    }

    return res.send({ email: user.email, id: user.id, username: user.username, role: user.role });
  }

  public static async delete(req: Request, res: Response): Promise<Response> {
    return res.send(await getRepository(User).delete(req.params.id));
  }

  public static async login(req: Request, res: Response): Promise<Response> {
    const UserRepository = getRepository(User)
    const user = await UserRepository.findOne({ username: req.body.username }, { relations: ['credential'] });
    if (user) {
      if (validPassword(req.body.password, user.credential.hash, user.credential.salt)) {
        await UserRepository.update(user.id, { lastLoginDate: new Date() });
        const token = issueJWT(user);
        const header = token.split(".")[0];
        const payload = token.split(".")[1];
        res.cookie('jwt', token, { httpOnly: true, secure: SECURE_COOKIE, maxAge: MAXAGE_COOKIE, domain: DOMAIN_COOKIE })
        res.cookie('payload', `${header}.${payload}`, { httpOnly: false, secure: SECURE_COOKIE, maxAge: MAXAGE_COOKIE, domain: DOMAIN_COOKIE })
        return res.send({ token: `Bearer ${token}`, username: user.username, emailVerified: user.emailVerified });
      } else {
        return res.sendStatus(401);
      }
    } else {
      return res.sendStatus(401);
    }
  }

  public static async resendVerificationEmail(req: Request, res: Response): Promise<Response> {
    if (!req.user) return res.sendStatus(401);

    const user = await getRepository(User).findOne({ id: req.user.id }, { relations: ['credential'] });
    if (!user) return res.sendStatus(404);

    const uniqueAuthenticationToken = generateUniqueAuthenticationToken(user);
    const template = (new EmailGenerator()).getHTML('VerifyEmail', {
      username: user.username,
      verifyLink: `${process.env.WEBAPP_URL}/verify-email/${uniqueAuthenticationToken.identity}/${uniqueAuthenticationToken.today}/${uniqueAuthenticationToken.token}`
    });
    const emailSender = new EmailSender(user.email, 'Verify', template);
    await emailSender.send();

    return res.sendStatus(200);
  }

  public static async passwordForgot(req: Request, res: Response): Promise<Response> {
    const email = req.body.email as string;
    if (!email) return res.sendStatus(400);

    const user = await getRepository(User).findOne({ email }, { relations: ['credential'] });
    if (user) {
      if(areEmailsEnabled()) {
        const uniqueAuthenticationToken = generateUniqueAuthenticationToken(user);
        const template = (new EmailGenerator()).getHTML('ResetPassword', {
          username: user.username,
          resetLink: `${process.env.WEBAPP_URL}/password-reset/${uniqueAuthenticationToken.identity}/${uniqueAuthenticationToken.today}/${uniqueAuthenticationToken.token}`
        });
        const emailSender = new EmailSender(user.email, 'Reset password', template);
        await emailSender.send();
      }
    }

    // We don't want the API user to know if a token was generated or not, we display success all the time.
    return res.sendStatus(200);
  }

  public static async passwordReset(req: Request, res: Response): Promise<Response> {
    const authenticatedUser = await authenticateUniqueAuthenticationToken(req.params.identity, req.params.today, req.params.token);
    
    if (!authenticatedUser) return res.sendStatus(401)
    if (!req.body.password) return res.sendStatus(400)

    const saltAndHash = generateSaltAndHash(req.body.password);
    await getRepository(UserCredential).update(authenticatedUser.credential.id, saltAndHash);

    return res.sendStatus(200);
  }

  public static async verifyEmail(req: Request, res: Response): Promise<Response> {
    const authenticatedUser = await authenticateUniqueAuthenticationToken(req.params.identity, req.params.today, req.params.token);
    if (!authenticatedUser) return res.sendStatus(401)
    await getRepository(User).update(authenticatedUser.id, { emailVerified: true });
    return res.sendStatus(200);
  }
}
