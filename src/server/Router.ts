import { Application, NextFunction, Request, Response } from "express";
import multer from 'multer';
import passport from "passport";
import { RouteController } from "./controllers/Route.controller";
import { UserController } from "./controllers/User.controller";
import ROUTES from "./routes";

const upload = multer();
const authorize = (req: Request, res: Response, next: NextFunction) => {
  passport.authenticate('jwt', { session: false }, (err, user, payload) => {
    if (req.route.path === ROUTES.RESEND_VERIFICATION_EMAIL && payload === 'EMAIL_NOT_VERIFIED') {
      req.user = user;
      console.log('special case mate!')
      return next();
    }
    if (err || !user) {
      return res.status(401).send(err);
    }
    req.user = user;
    return next();
  })(req, res, next);
}

export class Router {
  public static build(app: Application): void {
    app.route(ROUTES.HEALTH).get((req, res) => res.sendStatus(200));

    app.route(ROUTES.USER).post(UserController.create);
    app.route(ROUTES.USER).get(UserController.read);
    app.route(ROUTES.USER_BY_ID).get(UserController.readOne);
    app.route(ROUTES.USER_BY_ID).put(authorize, UserController.update);
    app.route(ROUTES.USER_BY_ID).delete(authorize, UserController.delete);
    app.route(ROUTES.LOGIN).post(UserController.login);
    app.route(ROUTES.PASSWORD_FORGOT).post(UserController.passwordForgot);
    app.route(ROUTES.PASSWORD_RESET).post(UserController.passwordReset);
    app.route(ROUTES.VERIFY_EMAIL).get(UserController.verifyEmail);
    app.route(ROUTES.RESEND_VERIFICATION_EMAIL).get(authorize, UserController.resendVerificationEmail);

    app.route(ROUTES.ROUTE).post(authorize, RouteController.create);
    app.route(ROUTES.ROUTE).get(RouteController.read);
    app.route(ROUTES.ROUTE_BY_ID).get(RouteController.readOne);
    app.route(ROUTES.ROUTE_GPX_UPLOAD).post(authorize, upload.single('file'), RouteController.gpxUpload);
    app.route(ROUTES.ROUTE_BY_ID).put(authorize, RouteController.update);
    app.route(ROUTES.ROUTE_BY_ID).delete(authorize, RouteController.delete);
    app.route(ROUTES.RATING).post(authorize, RouteController.rate);
  }
}
