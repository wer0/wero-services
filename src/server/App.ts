import express from "express";
import cors from 'cors';
import morgan from 'morgan';
import passport from "passport";
import { configurePassport } from "./Passport";
import { Router } from "./Router";
import cookieParser from 'cookie-parser';

export class App {
  public app: express.Application;

  constructor() {
    this.app = express();
    this.setup();
    Router.build(this.app);
  }

  private setup() {
    configurePassport(passport)
    this.app.use(cookieParser())
    this.app.use(passport.initialize());
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: true }));
    this.app.use(morgan('tiny'))
    this.app.use(cors({ credentials: true, origin: true }));
  }
}
