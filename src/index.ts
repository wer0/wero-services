import dotenv from 'dotenv';
dotenv.config();
import "reflect-metadata";
import { createConnection } from "typeorm";
import { App } from "./server/App";

const app = new App();
const PORT = Number(process.env.PORT) || 8080;
app.app.listen(PORT, () => console.log(`-> Listening on port ${PORT}.`));
createConnection().then(() => console.log(`-> Database connection successful.`));