import supertest from "supertest";

import { closeDatabaseConnection, createDatabaseConnection } from './databaseConnection';

import { App } from "../server/App";
import ROUTES from "../server/routes";
import { Roles } from "../server/Roles";
import { getRepository } from "typeorm";
import { User } from "../entity/User";

const app = new App();
const request = supertest(app.app);

beforeEach(createDatabaseConnection);
afterEach(closeDatabaseConnection);

test('We can create a user', async () => {
    const username = 'john.doe';
    const testCredentials =  { username, password: 'mysupersafepassword', email: 'john.doe@gmail.com' }
    const user = await request.post(ROUTES.USER).send(testCredentials);
    expect(user.body.token).toBeDefined();
    expect(user.body.username).toBeDefined();
    expect(user.body.username).toBe(username);
    expect(user.body.id).toBeDefined();
});

test('A user can edit itself, and only itself', async () => {
    const johnDoeCredentials = { username: 'john.doe', password: 'mysupersafepassword', email: 'john.doe@gmail.com' }
    const johnDoe = await request.post(ROUTES.USER).send(johnDoeCredentials);
    await getRepository(User).update({ id: johnDoe.body.id }, { emailVerified: true });
    expect(johnDoe.statusCode).toBe(200);

    const loisLaneCredentials = { username: 'lois.lane', password: 'myothersupersafepassword', email: 'lois.lane@gmail.com' }
    const loisLane = await request.post(ROUTES.USER).send(loisLaneCredentials);
    await getRepository(User).update({ id: loisLane.body.id }, { emailVerified: true });
    expect(loisLane.statusCode).toBe(200);

    const anonymousEditsJohn = await request.put(ROUTES.USER_BY_ID.replace(':id', johnDoe.body.id)).send({ username: 'random' });
    expect(anonymousEditsJohn.statusCode).toBe(401);

    const johnEditsJohn = await request.put(ROUTES.USER_BY_ID.replace(':id', johnDoe.body.id)).set('Authorization', johnDoe.body.token).send({ username: 'johnnydoe' });
    expect(johnEditsJohn.statusCode).toBe(200);

    const loisEditsJohn = await request.put(ROUTES.USER_BY_ID.replace(':id', johnDoe.body.id)).set('Authorization', loisLane.body.token).send({ username: 'johnnytheloser' });
    expect(loisEditsJohn.statusCode).toBe(401);
});

test('A user can login', async () => {
    const username = 'john.doe'
    const password = 'mysupersafepassword';
    const johnDoeCredentials = { username, password, email: 'john.doe@gmail.com' }
    const johnDoe = await request.post(ROUTES.USER).send(johnDoeCredentials);
    expect(johnDoe.statusCode).toBe(200);

    const loginJohn = await request.post(ROUTES.LOGIN).send({ username, password });
    expect(loginJohn.body.token).toBeDefined();
    expect(loginJohn.body.username).toBeDefined();
});

test('A user can change its password', async () => {
    const username = 'john.doe'
    const oldPassword = 'mysupersafepassword'
    const johnDoeCredentials = { username, password: oldPassword, email: 'john.doe@gmail.com' }
    const johnDoe = await request.post(ROUTES.USER).send(johnDoeCredentials);
    await getRepository(User).update({ id: johnDoe.body.id }, { emailVerified: true });
    expect(johnDoe.statusCode).toBe(200);

    const newPassword = 'anewpassword';
    const editJohnsPassword = await request.put(ROUTES.USER_BY_ID.replace(':id', johnDoe.body.id)).set('Authorization', johnDoe.body.token).send({ newPassword, oldPassword });
    expect(editJohnsPassword.statusCode).toBe(200);

    const loginJohnWithNewPassword = await request.post(ROUTES.LOGIN).send({ username, password: newPassword });
    expect(loginJohnWithNewPassword.body.token).toBeDefined();
    expect(loginJohnWithNewPassword.body.username).toBeDefined();
});

test('A user can change its username', async () => {
    const username = 'john.doe'
    const johnDoeCredentials = { username, password: 'mysupersafepassword', email: 'john.doe@gmail.com' }
    const johnDoe = await request.post(ROUTES.USER).send(johnDoeCredentials);
    await getRepository(User).update({ id: johnDoe.body.id }, { emailVerified: true });
    expect(johnDoe.statusCode).toBe(200);

    const newUsername = 'johnny';
    const editJohnsUsername = await request.put(ROUTES.USER_BY_ID.replace(':id', johnDoe.body.id)).set('Authorization', johnDoe.body.token).send({ username: newUsername });
    expect(editJohnsUsername.statusCode).toBe(200);

    expect(editJohnsUsername.body.username).toEqual(newUsername);
    expect(editJohnsUsername.body.id).toEqual(johnDoe.body.id);
});

test('A user can change its email', async () => {
    const username = 'john.doe'
    const johnDoeCredentials = { username, password: 'mysupersafepassword', email: 'john.doe@gmail.com' }
    const johnDoe = await request.post(ROUTES.USER).send(johnDoeCredentials);
    await getRepository(User).update({ id: johnDoe.body.id }, { emailVerified: true });
    expect(johnDoe.statusCode).toBe(200);

    const newEmail = 'johnnydoe@gmail.com';
    const editJohnsUsername = await request.put(ROUTES.USER_BY_ID.replace(':id', johnDoe.body.id)).set('Authorization', johnDoe.body.token).send({ email: newEmail });
    expect(editJohnsUsername.statusCode).toBe(200);
    expect(editJohnsUsername.body.email).toEqual(newEmail);
    expect(editJohnsUsername.body.id).toEqual(johnDoe.body.id);
});

test('We can get a list of users', async () => {
    const username = 'john.doe';
    const johnDoeCredentials = { username, password: 'mysupersafepassword', email: 'john.doe@gmail.com' }
    const johnDoe = await request.post(ROUTES.USER).send(johnDoeCredentials);
    await getRepository(User).update({ id: johnDoe.body.id }, { emailVerified: true });
    expect(johnDoe.statusCode).toBe(200);

    const users = await request.get(ROUTES.USER);
    expect(users.statusCode).toBe(200);
    expect(users.body.length).toBe(1);
    expect(users.body[0]).toEqual({ id: 1, username, role: Roles.USER });
});

test('We can get one user', async () => {
    const username = 'john.doe';
    const johnDoeCredentials = { username, password: 'mysupersafepassword', email: 'john.doe@gmail.com' }
    const johnDoe = await request.post(ROUTES.USER).send(johnDoeCredentials);
    await getRepository(User).update({ id: johnDoe.body.id }, { emailVerified: true });
    expect(johnDoe.statusCode).toBe(200);

    const users = await request.get(ROUTES.USER_BY_ID.replace(':id', johnDoe.body.id));
    expect(users.statusCode).toBe(200);
    expect(users.body).toEqual({ id: 1, username, role: Roles.USER, emailVerified: true });
});

test('We can delete one user', async () => {
    const username = 'john.doe';
    const johnDoeCredentials = { username, password: 'mysupersafepassword', email: 'john.doe@gmail.com' }
    const johnDoe = await request.post(ROUTES.USER).send(johnDoeCredentials);
    await getRepository(User).update({ id: johnDoe.body.id }, { emailVerified: true });
    expect(johnDoe.statusCode).toBe(200);

    const deleteJohn = await request.delete(ROUTES.USER_BY_ID.replace(':id', johnDoe.body.id)).set('Authorization', johnDoe.body.token)
    expect(deleteJohn.statusCode).toBe(200);
});

test('We can delete one user and we expect count', async () => {
    const UserCredentialRepository = getRepository(User);

    expect(await UserCredentialRepository.count()).toEqual(0);

    const username = 'john.doe';
    const johnDoeCredentials = { username, password: 'mysupersafepassword', email: 'john.doe@gmail.com' }
    const johnDoe = await request.post(ROUTES.USER).send(johnDoeCredentials);
    await getRepository(User).update({ id: johnDoe.body.id }, { emailVerified: true });
    expect(johnDoe.statusCode).toBe(200);

    expect(await UserCredentialRepository.count()).toEqual(1);

    const deleteJohn = await request.delete(ROUTES.USER_BY_ID.replace(':id', johnDoe.body.id)).set('Authorization', johnDoe.body.token)
    expect(deleteJohn.statusCode).toBe(200);

    expect(await UserCredentialRepository.count()).toEqual(0);
});

// test('A user can forget his password', async () => {
//     const username = 'john.doe';
//     const johnDoeCredentials = { username, password: 'mysupersafepassword', email: 'john.doe@gmail.com' }
//     const johnDoe = await request.post(ROUTES.USER).send(johnDoeCredentials);
//     expect(johnDoe.statusCode).toBe(200);

//     const passwordForgot = await request.post(ROUTES.PASSWORD_FORGOT).send({ email: 'john.doe@gmail.com'  })
//     expect(passwordForgot.statusCode).toBe(200);

//     const passwordReset = await request.post('/password-reset/' + passwordForgot.body.token).send({
//         password: 'anewpassword'
//     })
//     expect(passwordReset.statusCode).toBe(200);

//     const loginJohnWithNewPassword = await request.post(ROUTES.LOGIN).send({ username, password: 'anewpassword' });
//     expect(loginJohnWithNewPassword.statusCode).toBe(200);
//     expect(loginJohnWithNewPassword.body.token).toBeDefined();
//     expect(loginJohnWithNewPassword.body.expires).toBeDefined();
//     expect(loginJohnWithNewPassword.body.username).toBeDefined();
// });