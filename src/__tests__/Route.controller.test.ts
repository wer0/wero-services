import supertest from "supertest";

import { closeDatabaseConnection, createDatabaseConnection } from './databaseConnection';

import { App } from "../server/App";
import ROUTES from "../server/routes";
import { User } from "../entity/User";
import { getRepository } from "typeorm";

const app = new App();
const request = supertest(app.app);

beforeEach(createDatabaseConnection);
afterEach(closeDatabaseConnection);

async function createUserAndGetToken(): Promise<string> {
  const testCredentials =  { username: 'john.doe', password: 'mysupersafepassword', email: 'john.doe@gmail.com' }
  const user = await request.post(ROUTES.USER).send(testCredentials);
  await getRepository(User).update({ id: user.body.id }, { emailVerified: true });
  return user.body.token;
}

async function createOtherUserAndGetToken(): Promise<string> {
  const testCredentials =  { username: 'mike.doe', password: 'mysupersafepassword', email: 'mike.doe@gmail.com' }
  const user = await request.post(ROUTES.USER).send(testCredentials);
  await getRepository(User).update({ id: user.body.id }, { emailVerified: true });
  return user.body.token;
}


test("CREATE a route, UPLOAD a GPX to the route, and GET the route", async () => {
  const bearerToken = await createUserAndGetToken();
  const response = await request.post(ROUTES.ROUTE).set('Authorization', bearerToken).send({ name: 'My First Route', description: 'An awesome route', userId: 'someuserid' });
  expect(response.statusCode).toBe(201);
  expect(response.body.id).toBeDefined();
  const gpxResponse = await request.post(ROUTES.ROUTE_GPX_UPLOAD.replace(':id', response.body.id)).set('Authorization', bearerToken).field('hello', 'world').attach('file', 'src/__tests__/resources/paris-ozoir.gpx');
  expect(gpxResponse.statusCode).toBe(200);
  const readResponse = await request.get(ROUTES.ROUTE_BY_ID.replace(':id', response.body.id));
  expect(readResponse.statusCode).toBe(200);
  // These are the values we except from computing the GPX File to GeoJSON and to elevationGain/altitude for our test file
  expect(readResponse.body.elevationGain).toBe(699);
  expect(readResponse.body.distance).toBe(107061);
});

test("CREATE a route, and rate the route", async () => {
  const bearerToken = await createUserAndGetToken();
  const otherBearerToken = await createOtherUserAndGetToken();
  const firstRoute = await request.post(ROUTES.ROUTE).set('Authorization', bearerToken).send({ name: 'My First Route', description: 'An awesome route', userId: 'someuserid' });
  
  const rateResponse = await request.post(ROUTES.RATING.replace(':id', firstRoute.body.id)).set('Authorization', bearerToken).send({ rating: 5 });
  expect(rateResponse.statusCode).toBe(200);
  const readAfterRatingResponse = await request.get(ROUTES.ROUTE_BY_ID.replace(':id', firstRoute.body.id));
  expect(readAfterRatingResponse.body.rating).toBe(5);
  expect(readAfterRatingResponse.statusCode).toBe(200);

  const rateAgainResponse = await request.post(ROUTES.RATING.replace(':id', firstRoute.body.id)).set('Authorization', bearerToken).send({ rating: 5 });
  expect(rateAgainResponse.statusCode).toBe(200);

  const rateAgainByDifferentUserResponse = await request.post(ROUTES.RATING.replace(':id', firstRoute.body.id)).set('Authorization', otherBearerToken).send({ rating: 4 });
  expect(rateAgainByDifferentUserResponse.statusCode).toBe(200);

  const readAgainAfterRatingResponse = await request.get(ROUTES.ROUTE_BY_ID.replace(':id', firstRoute.body.id));
  expect(readAgainAfterRatingResponse.body.rating).toBe(5);

  const secondRoute = await request.post(ROUTES.ROUTE).set('Authorization', bearerToken).send({ name: 'My Second Route', description: 'An awesome route', userId: 'someuserid' });
  const secondRouteRateResponse = await request.post(ROUTES.RATING.replace(':id', secondRoute.body.id)).set('Authorization', bearerToken).send({ rating: 5 });
  expect(secondRouteRateResponse.statusCode).toBe(200);
});

test("UPDATE a route", async () => {
  const bearerToken = await createUserAndGetToken();
  const response = await request.post(ROUTES.ROUTE).set('Authorization', bearerToken).send({ name: 'My First Route', description: 'An awesome route', userId: 'someuserid' });
  expect(response.statusCode).toBe(201);
  expect(response.body.id).toBeDefined();
  const newDescription = 'A boring route, please...'
  const updateResponse = await request.put(ROUTES.ROUTE_BY_ID.replace(':id', response.body.id)).set('Authorization', bearerToken).send({
    description: newDescription
  });
  expect(updateResponse.body.description).toEqual(newDescription);
})

test("DELETE a route", async () => {
  const bearerToken = await createUserAndGetToken();
  const response = await request.post(ROUTES.ROUTE).set('Authorization', bearerToken).send({ name: 'My First Route', description: 'An awesome route', userId: 'someuserid' });
  expect(response.statusCode).toBe(201);
  expect(response.body.id).toBeDefined();
  const deleteResponse = await request.delete(ROUTES.ROUTE_BY_ID.replace(':id', response.body.id)).set('Authorization', bearerToken)
  expect(deleteResponse.statusCode).toBe(200);
})

test("DELETE a user after creating a route", async () => {
  const username = 'john.doe';
  const testCredentials =  { username, password: 'mysupersafepassword', email: 'john.doe@gmail.com' }
  const user = await request.post(ROUTES.USER).send(testCredentials);
  await getRepository(User).update({ id: user.body.id }, { emailVerified: true });

  const response = await request.post(ROUTES.ROUTE).set('Authorization', user.body.token).send({ name: 'My First Route', description: 'An awesome route', userId: 'someuserid' });
  expect(response.statusCode).toBe(201);
  expect(response.body.id).toBeDefined();

  const deleteUser = await request.delete(ROUTES.USER_BY_ID.replace(':id', user.body.id)).set('Authorization', user.body.token)
  expect(deleteUser.statusCode).toBe(200);

  const getResponse = await request.get(ROUTES.ROUTE_BY_ID.replace(':id', response.body.id))
  expect(getResponse.statusCode).toBe(200);
})

test("GET a route", async () => {
  const bearerToken = await createUserAndGetToken();
  const response = await request.post(ROUTES.ROUTE).set('Authorization', bearerToken).send({ name: 'My First Route', description: 'An awesome route', userId: 'someuserid' });
  expect(response.statusCode).toBe(201);
  expect(response.body.id).toBeDefined();
  const getResponse = await request.get(ROUTES.ROUTE_BY_ID.replace(':id', response.body.id)).set('Authorization', bearerToken)
  expect(getResponse.statusCode).toBe(200);
})

test("GET routes", async () => {
  const bearerToken = await createUserAndGetToken();
  const response = await request.post(ROUTES.ROUTE).set('Authorization', bearerToken).send({ name: 'My First Route', description: 'An awesome route', userId: 'someuserid' });
  expect(response.statusCode).toBe(201);
  expect(response.body.id).toBeDefined();
  const getResponse = await request.get(ROUTES.ROUTE).set('Authorization', bearerToken)
  expect(getResponse.statusCode).toBe(200);
  expect(getResponse.body.length).toBe(1);
})