import {
  ConnectionOptions,
  createConnection,
  getConnection,
} from "typeorm";
import { Route } from "../entity/Route";
import { RouteRating } from "../entity/RouteRating";
import { User } from "../entity/User";
import { UserCredential } from "../entity/UserCredential";
import { RouteRatingSubscriber } from "../subscriber/RouteRatingSubscriber";

export async function createDatabaseConnection(): Promise<void> {
  const options: ConnectionOptions = {
    type: "sqlite",
    database: ":memory:",
    dropSchema: false,
    synchronize: true,
    logging: false,
    entities: [User, Route, RouteRating, UserCredential],
    subscribers: [RouteRatingSubscriber]
  };
  await createConnection(options);
  return;
}

export async function closeDatabaseConnection(): Promise<void> {
  await getConnection().close();
}
