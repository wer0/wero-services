WERO Services
==
A backend API for WERO.

Main concepts
-
- A library for popular and quality cycle routes
- A platform to organize challenges on said routes

Concepts Principaux
-
- Une bibliothèque d'itinéraires cyclables populaires et de qualité
- Une plateforme pour organiser des challenges sur ces itinéraires

Docker setup
-
### Overview
- Web service available on `http://localhost:8080/`
- Database viewable / editable through PgAdmin on `http://localhost:8081/`.
Credentials: `admin@mail.com / postgres`

### Build & Launch
- `docker-compose build` (once)
- `docker-compose up` (launch postgres database & pgadmin)
- `docker-compose run --rm --service-ports web` to launch web service container
    - `npm ci` to clean install dependencies
    - `npm run dev` to start Node in development mode