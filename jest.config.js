module.exports = {
  testMatch: ['**/?(*.)+(spec|test).ts'],
  transform: {
    "^.+\\.(ts|tsx)$": "ts-jest",
  },
  maxWorkers: 1,
  testEnvironment: "node",
};
