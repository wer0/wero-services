module.exports = {
   "type": "postgres",
   "host": process.env.POSTGRES_HOST || 'localhost',
   "port": 5432,
   "username": process.env.POSTGRES_USER || 'postgres',
   "password": process.env.POSTGRES_PASSWORD || 'password',
   "database": process.env.POSTGRES_DB || 'wero-db',
   "synchronize": true,
   "logging": false,
   entities: [`${__dirname}/lib/entity/**/*.js`],
   migrations: [`${__dirname}/lib/migration/**/*.js`],
   subscribers: [`${__dirname}/lib/subscriber/**/*.js`],
   "cli": {
      "entitiesDir": "src/entity",
      "migrationsDir": "src/migration",
      "subscribersDir": "src/subscriber"
   }
}